import React, { useState } from "react";
import { View, StyleSheet, Image,Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Container from "../components/container";
import { PrimaryTheme } from "../styles/Theme";
import { heightPercentageToDP } from "react-native-responsive-screen";
import CustomButton from "../components/CustomButton";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { utils } from "../utils/utils";
import { ScreenNames } from "./Dashboard";
import Bookings from "./Booking";
    interface Props{
        route:any
        navigation:any
      }
const CBookings = (props:Props) => {


      const Booking=()=>{
        props.navigation.navigate(ScreenNames.BOOKINGSREEN)
    }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
       <Container>
         <View style={styles.sectionStyle}>
         <View style={{padding:18}}>
             <Image source={utils.Img.CBookings}/>
           </View>
         </View>
         <CustomButton onPress={Booking} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Booking confirm'}/>
       </Container>
    </ScrollView>
  );
};
 
const styles = StyleSheet.create({

  buttonStyleC:{
    backgroundColor: PrimaryTheme.$MY_Theme,
    borderWidth:0,
    borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%'),
    marginTop:hp('3%')
  },
  sectionStyle: {
    marginTop:hp('8%'),
   },
  });


export default CBookings;