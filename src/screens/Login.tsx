import React from "react";
import { Image, StatusBar, StyleSheet, Text, View,Dimensions } from "react-native";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import ImageOverlay from "react-native-image-overlay";
import CustomButton from "../components/CustomButton";
import { ScreenName } from "../utils/Navigations/Routes";
import { utils } from "../utils/utils";
import {widthPercentageToDP as wp, heightPercentageToDP as hp,listenOrientationChange as lor,removeOrientationListener as rol, widthPercentageToDP, heightPercentageToDP,} from 'react-native-responsive-screen';
import {useOrientation} from '../components/useOrientation'
import { ScreenNames } from "./Dashboard";
interface Props{
  route:any
  navigation:any
  orientation:any
}

const Login=(props:Props)=>{

  const orientation = useOrientation();
  
    const SignIn = () =>{
    props.navigation.navigate(ScreenName.SIGNIN)
    }
    const SignUp = () =>{
      props.navigation.navigate(ScreenName.SIGNUP)
    }
  return(
    <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1,
    }}>
      <View style={{flexDirection:'row'}}>
        <StatusBar
        animated={true}
        backgroundColor="#0A5688"
        />
          <ImageOverlay        
            containerStyle={[orientation === 'portrait' ? Pstyles.image : Lstyles.image]}
            overlayColor="#0A5688"
            overlayAlpha={0.8}
            source={orientation === 'portrait' ? utils.Img.Background : null}>
          <View style={[orientation === 'portrait' ? Pstyles.Brand_Bg : Lstyles.Brand_Bg]}>
              <Image style={[orientation === 'portrait' ? Pstyles.LoginImage : Lstyles.LoginImage]} source={orientation === 'portrait' ? utils.Img.LoginScreen : utils.Img.LoginScreen}/>  
              <Text style={[orientation === 'portrait' ? Pstyles.text : Lstyles.text]}>Book an Appointment for Salon Barber.</Text>
              <CustomButton onPress={SignIn} title={'Sign In'}/>
              <CustomButton onPress={SignUp} textStyle={{color:'#fff'}} buttonStyle={[orientation === 'portrait' ? Pstyles.buttonStyle : Lstyles.buttonStyle]} title={'Sign Up'}/>
          </View>
          </ImageOverlay>
      </View>
      </ScrollView>
        )
}





const Pstyles = StyleSheet.create({

  image: {
    flex: 1,
    height:hp('100%'),
    
  },
  Brand_Bg:{
    alignItems:'center',
    marginTop:140,
    borderRadius:12,
    opacity:0.8,
    padding:8,
  },
  text: {
    color: "white",backgroundColor: 'transparent',
    fontSize: 27,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom:90
  },
  buttonStyle:{
    backgroundColor: 'transparent',
    borderWidth:1,
    borderColor:'#fff',
  },
  LoginImage:{
    width:110,
    height:110
  }
});

const Lstyles = StyleSheet.create({

  image: {
    ...Pstyles.image,
    height: hp('53%'),
    },
  Brand_Bg:{
    alignItems:'center',
    borderRadius:12,
    opacity:0.8,
    padding:8,
  },
  text: {
    color: "#fff",
    fontSize: 27,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom:12,
    width:wp('90%'),
    marginTop:12,
  },
  buttonStyle:{
    backgroundColor: 'transparent',
    borderWidth:1,
    borderColor:'#fff',
  },
  LoginImage:{
  width:wp('40%'),
  height:hp('15%'),
  
}
});


export default Login;