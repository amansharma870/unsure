import { utils } from "../utils/utils";

export const MainSlider= [
    [
        { id: 1, image: (utils.Img.CrausalA), title: "Cut makes you Perfect", subTitle:"SCISSOR PRIDE TWIN SALON",Paragraph:"25% OFF on Haircuts"},
        { id: 2, image: (utils.Img.CrausalB),title: "Treditional Cuts", subTitle:"TRADITIONAL TWIN SALON",Paragraph:"30% OFF on Treditional Haircuts"},
        { id: 3, image: (utils.Img.CrausalC),title: "Proffesional Cuts", subTitle:"PRIDE PROFFESIONAL CUTS",Paragraph:"10% OFF on Proffesional cuts"},
        { id: 4, image: (utils.Img.CrausalD),title: "Own Style Salon", subTitle:"Style Made for You",Paragraph:"20% OFF on Haircuts"},
    ],
    [
        { id: 3, image: (utils.Img.CrausalA3), subTitle: "Hands", subPoint:"88 Places"},
        { id: 4, image: (utils.Img.CrausalA4), subTitle: "Lips", subPoint:"22 Places"},
        { id: 5, image: (utils.Img.CrausalA1), subTitle: "Hair", subPoint:"85 Places"},
        { id: 6, image: (utils.Img.CrausalA2), subTitle: "Skin", subPoint:"100 Places"},
        { id: 3, image: (utils.Img.CrausalA3), subTitle: "Hands", subPoint:"88 Places"},
        { id: 4, image: (utils.Img.CrausalA4), subTitle: "Lips", subPoint:"22 Places"},
    ],
    [
        { id: 1, image: (utils.Img.salonPlaceA),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 2, image: (utils.Img.salonPlaceB),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 3, image: (utils.Img.salonPlaceC),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 4, image: (utils.Img.salonPlaceD),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 5, image: (utils.Img.salonPlaceE),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
    ],
    [
        { id: 1, image: (utils.Img.BarberA),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 2, image: (utils.Img.BarberB),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 3, image: (utils.Img.BarberC),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
        { id: 4, image: (utils.Img.BarberD),Fname:'Redbox Barber',place:'460, 27th Main Rd..',star:'4.5'},
    ],
    [
        { id: 1, image: (utils.Img.PakageA), subTitle: "Bridal Makeup",},
        { id: 2, image: (utils.Img.PakageB), subTitle: "Hair and Care",},
        { id: 3, image: (utils.Img.PakageC), subTitle: "Shave and trim",},
        { id: 4, image: (utils.Img.PakageD), subTitle: "Style hair",}
    ],
    [
        { id: 1, image: (utils.Img.ModelA)},
        { id: 2, image: (utils.Img.ModelB)},
        { id: 3, image: (utils.Img.ModelC)},
        { id: 4, image: (utils.Img.ModelD)}
    ],

]
