import React from 'react'
import SplashScreen from 'react-native-splash-screen';
import {Navigators} from './src/utils/Navigations/Navigator'

interface Props{

}
interface State{

}

export class App extends React.Component<Props,State>{
  constructor(props){
    super(props);
    this.state={}
  }
  componentDidMount() {
      setTimeout(() => {
        SplashScreen.hide();
    }, 100)
  }

  render(){
   return <Navigators />
  }
  
}

export default App;
