import React, { useState } from "react";
import { View, StyleSheet, Image } from "react-native";
import RadioButtonRN from 'radio-buttons-react-native';
import { ScrollView } from "react-native-gesture-handler";
import Container from "../components/container";
import { utils } from "../utils/utils";
import Icon from "../components/Icon";
import { PrimaryTheme } from "../styles/Theme";
import { heightPercentageToDP } from "react-native-responsive-screen";
import CustomButton from "../components/CustomButton";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { ScreenNames } from "./Dashboard";
interface Props{
  route:any
  navigation:any
}
const Payment = (props:Props) => {
  const CartScreenB=()=>{
    props.navigation.navigate(ScreenNames.PAYMENT)
}
  const data = [
    {
      label: 'Paytm',
     },
     {
      label: 'Gpay'
     },
     {
      label: 'Net Banking',
     }
    ];

 
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
       <Container>
         <View style={styles.sectionStyle}>
           <View>
             <Image source={utils.Img.payment_logo}/>
           </View>
           <View style={{marginTop:heightPercentageToDP('15%')}}>
            <RadioButtonRN
                data={data}
                selectedBtn={(e) => console.log(e)}
                boxStyle={{height:35,borderWidth:0,backgroundColor:'#fff'}}
                circleSize={10}
                activeColor={PrimaryTheme.$MY_Theme}
                textColor={PrimaryTheme.$MY_Theme}
                initial={1}
              />
           </View>
         </View>
         <CustomButton onPress={CartScreenB} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Pay ₹ 600'}/>
       </Container>
    </ScrollView>

  );
};
 
const styles = StyleSheet.create({

buttonStyleC:{
  backgroundColor: PrimaryTheme.$MY_Theme,
  borderWidth:0,
  borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%'),
  marginTop:hp('3%')
},
sectionStyle: {
  marginTop:hp('1%')
 },
});


export default Payment;
