import  React from 'react';
import { ImageStyle, TextStyle, ViewStyle ,Platform} from 'react-native';
  export class utils{
        static image: any;
        static isIos() {
            throw new Error('Method not implemented.');
        }
        static Img={
            Background:require('../assets/Images/Login_BG.png'),
            LoginScreen:require('../assets/Images/App_Logo.png'),
            SignIn:require('../assets/Images/Sign_In.png'),
            SignUpA:require('../assets/Images/Man.png'),
            SignUpB:require('../assets/Images/woman.png'),
            Verify:require('../assets/Images/verify_account.png'),
            HomeScreenA:require('../assets/Images/Login_BG.png'),
            HomeScreenB:require('../assets/Images/App_Logo.png'),
            HomeScreenC:require('../assets/Images/Login_BG.png'),
            HomeScreenD:require('../assets/Images/App_Logo.png'),
            CrausalA:require('../assets/Images/CrausalA.jpeg'),
            CrausalB:require('../assets/Images/CrausalB.jpeg'),
            CrausalC:require('../assets/Images/CrausalC.jpeg'),
            CrausalD:require('../assets/Images/CrausalD.jpeg'),
            CrausalA1:require('../assets/Images/CrausalA1.png'),
            CrausalA2:require('../assets/Images/CrausalA2.png'),
            CrausalA3:require('../assets/Images/CrausalA3.png'),
            CrausalA4:require('../assets/Images/CrausalA4.png'),
            CBookings:require('../assets/Images/CBooking.png'),
            salonPlaceA:require('../assets/Images/salonPlaceA.jpg'),
            salonPlaceB:require('../assets/Images/salonPlaceB.jpg'),
            salonPlaceC:require('../assets/Images/salonPlaceC.jpg'),
            salonPlaceD:require('../assets/Images/salonPlaceD.jpg'),
            salonPlaceE:require('../assets/Images/salonPlaceE.jpg'),
            BarberA:require('../assets/Images/BarberA.jpg'),
            BarberB:require('../assets/Images/BarberB.jpg'),
            BarberC:require('../assets/Images/BarberC.jpg'),
            BarberD:require('../assets/Images/BarberD.jpeg'),
            BarberE:require('../assets/Images/BarberE.png'),
            PakageA:require('../assets/Images/PakageA.png'),
            PakageB:require('../assets/Images/PakageB.png'),
            PakageC:require('../assets/Images/pakageC.jpg'),
            PakageD:require('../assets/Images/PakageD.jpg'),
            PwdCng:require('../assets/Images/pwdChange.png'),
            Profile:require('../assets/Images/profile.png'),
            discountA:require('../assets/Images/discount.jpg'),
            discountB:require('../assets/Images/discountB.png'),
            discountC:require('../assets/Images/discountC.jpg'),
            discountD:require('../assets/Images/discountD.jpg'),
            ModelA:require('../assets/Images/modelA.jpeg'),
            ModelB:require('../assets/Images/modelB.jpeg'),
            ModelC:require('../assets/Images/modelC.jpeg'),
            ModelD:require('../assets/Images/modelD.jpeg'),
            SalonA:require('../assets/Images/salonsA.png'),
            SalonB:require('../assets/Images/salonsB.png'),
            SalonC:require('../assets/Images/ChocoSalonC.jpg'),
            mySalonA:require('../assets/Images/mySalonA.png'),
            mySalonB:require('../assets/Images/mySalonB.png'),
            mySalonC:require('../assets/Images/mySalonC.png'),
            mySalonD:require('../assets/Images/mySalonD.jpg'),
            mySalonE:require('../assets/Images/mySalonE.jpg'),
            ChocoSalonA:require('../assets/Images/ChocoSalonA.jpg'),
            ChocoSalonB:require('../assets/Images/ChocoSalonB.jpg'),
            ChocoSalonC:require('../assets/Images/ChocoSalonC.jpg'),
            ChocoSalonD:require('../assets/Images/ChocoSalonD.jpg'),
            ChocoSalonE:require('../assets/Images/ChocoSalonE.jpg'),
            ScissorManA:require('../assets/Images/ScissorManA.jpg'),
            ScissorManB:require('../assets/Images/ScissorManB.jpg'),
            ScissorManC:require('../assets/Images/ScissorManC.jpg'),
            ScissorManD:require('../assets/Images/ScissorManD.jpg'),
            ScissorManE:require('../assets/Images/ScissorManE.jpg'),
            payment_logo:require('../assets/Images/undraw_payments.png'),
            payments_logo:require('../assets/Images/payments_logo.png'),
            payment_Cash:require('../assets/Images/undraw_cash.png'),
            payment_MasterCard:require('../assets/Images/maestro-card.png'),
            payment_Paytm:require('../assets/Images/undraw_paytm.png'),
        }
        static dynamicStyle(
            pStyles: 
            ViewStyle
        |   ViewStyle[]
        |   TextStyle
        |   TextStyle[]
        |   ImageStyle
        |   ImageStyle[],
            lStyles:
            ViewStyle 
        |   ViewStyle[] 
        |   TextStyle 
        |   TextStyle[]
        |   ImageStyle 
        |   ImageStyle[],
            orientation:string 
            ){
        return   orientation === "portrait" ? pStyles:lStyles
    }

}      
     