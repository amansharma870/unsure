import * as React from 'react';
import { Text, View,Image,StyleSheet, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import { utils } from '../utils/utils';
import Icon from './Icon';

const FData = ()=>{
    return (
        <>
            <View style={{flexDirection:"row",justifyContent:"space-between",borderBottomWidth:0.5,paddingBottom:20,marginBottom:10}}>
                <View style={{flexDirection:"row"}}>
                    <Image style={{width:wp('30%'),height:hp('12%')}} 
                    resizeMode={'contain'} source={utils.Img.salonPlaceB} 
                    />
                <View style={{marginLeft:10}}>
                    <Text style={[Typography.heading,{fontSize:wp('3.7%'),color:PrimaryTheme.$MY_Theme,marginBottom:5}]}>Orange The Salon</Text>
                        <Text style={[Typography.subheading,{fontSize:wp('4.5%'),color:PrimaryTheme.$MY_Theme,marginBottom:10}]}>
                            Haircut  
                        </Text>
                    <View style={{flexDirection:"row"}}>
                        <Text style={[Typography.lighText,,{fontSize:15,marginRight:10}]}>
                            From ₹ 200
                        </Text>
                        <Icon name={'star'} size={15} 
                          color={PrimaryTheme.$MY_Theme}
                      />  
                      <Icon name={'star'} size={15} 
                          color={PrimaryTheme.$MY_Theme}
                      />  
                      <Icon name={'star'} size={15} 
                          color={PrimaryTheme.$MY_Theme}
                      />  
                      <Icon name={'star'} size={15} 
                          color={PrimaryTheme.$MY_Theme}
                      />  
                      <Icon name={'star'} size={15} 
                          color={PrimaryTheme.$MY_Theme}
                      />  
                      <Text style={{marginLeft:9}}>58</Text>
                    </View>
                </View>
                </View>
 
                <View style={{justifyContent:"space-between",paddingRight:15}}>
                  <View>
                    <Icon name={'heart'} size={20} 
                        style={{backgroundColor: PrimaryTheme.$MY_Theme,borderRadius:10,
                            paddingTop: 2,paddingRight: 2,paddingLeft: 2,}}
                        color='#FFF'
                    /> 
                  </View>
                </View>
               
                 </View> 
                
        </>
    )
}
const Style = StyleSheet.create({
    ButtonTime:{
      backgroundColor:PrimaryTheme.$MY_Theme,
      width:wp('30%'),
      borderRadius: 10,
      height: hp('6.5%'),
      fontSize:14,
      padding:9,
      letterSpacing:1, 
      marginRight:10
  },
    })
export default FData;

