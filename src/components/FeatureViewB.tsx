import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScreenNames } from '../screens/Dashboard';
import { Typography } from '../styles/Global';

interface Props{
    navigation: any;
    onPress:any;
    FeatureName:any;
    FeatureNameB:any;
}
const FeatureViewB=(props:Props)=> {
        return (
            <View style={{flexDirection:"row",justifyContent:'space-between',width:wp('93%')}}>
                <Text style={[Typography.title,{fontSize:18.9, fontWeight:'900',}]}>{props.FeatureName}</Text>
            <TouchableOpacity onPress={()=> props.navigation.navigate(ScreenNames.MAP)}>
                <Text style={[Typography.title,{fontSize:17, fontWeight:'900',color:'red'}]}>{props.FeatureNameB}</Text>
            </TouchableOpacity>
            </View>
        )
    }

export default FeatureViewB

const styles = StyleSheet.create({
    FeatureStyle:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:'80%',
        marginTop:hp('1%')
    },
    FeatureText:{
        color:'#000',
        fontWeight:'bold'
    },
    FeatureTextB:{
        color:'#0A5688',
        fontWeight:'bold'
    }
})
