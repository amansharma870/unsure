import * as React from 'react';
import { View,StyleSheet,Text, StatusBar,ActivityIndicator, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Icon from '../components/Icon';
import { Inputs_Area, Typography } from '../styles/Global';
import { ScreenName } from '../utils/Navigations/Routes';
import Slider from '../components/ImageSliderA';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { MainSlider } from '../styles/SliderData';
import Container from '../components/container';
import SliderB from '../components/ImageSliderB';
import { useState } from 'react';
import SliderC from '../components/ImageSliderC';
import { ScreenNames } from './Dashboard';
import { useOrientation } from '../components/useOrientation';
import { PrimaryTheme } from '../styles/Theme';

interface Props{
    route:any
    navigation:any
  }
const Home = (props:Props) =>{
  const orientation = useOrientation();

  const [loading, setLoading] = useState(true);
    setTimeout(() => {
      setLoading(false);
    }, 100);
    const Service=()=>{
      props.navigation.navigate(ScreenNames.SERVICES)
  }
    const Map = () =>{
        props.navigation.navigate(ScreenName.Map)
        }
        const TopServices = () =>{
          props.navigation.navigate(ScreenNames.TOPSERVICES)
          }
          const Salons = () =>{
            props.navigation.navigate(ScreenNames.BESTSALONS)
            }
            const Stylists = () =>{
              props.navigation.navigate(ScreenNames.STYLISTS)
              }
              
                
return(
    <Container>
      <StatusBar
        animated={false}
        backgroundColor="#0A5688"
        />
        
        {loading ? (
          <ActivityIndicator
            color={'#0A5688'} size={55} 
          />
        ) :
            <View style={{paddingBottom:0}}>
              
              <View style={styles.headermap}>
                    <View style={{flexDirection:"row",}}>
                      <TouchableOpacity onPress={Map}>
                          <Icon name={'location-sharp'} size={25} style={{marginRight:12}}
                          color={PrimaryTheme.$DARK_COLOR}
                        />  
                      </TouchableOpacity>  
                        <Text style={[Typography.heading,{fontSize:16,color:PrimaryTheme.$DARK_COLOR}]}>High Rd, University</Text>   
                    </View>
                  <View>
                    <Icon  name={'notifications-outline'} size={26}
                        color={PrimaryTheme.$DARK_COLOR}
                      />  
                  </View>   
              </View> 
        <ScrollView showsVerticalScrollIndicator={false}>
            
            
              <View style={styles.sectionStyle}>
                <TextInput 
                  style={styles.SearchBar}
                  placeholder="Search for salons, services.."
                />
                <Icon  name={'search-outline'} size={18} style={{paddingTop:wp('2.5%'),paddingRight:wp('3%')}}
                      color={'#686868'}
                />  
              </View>
              <View > 
                <Slider 
                  onPress={Service}
                  images={MainSlider[0]}
                  SliderImage={[orientation === 'portrait' ? styles.Image : Lstyles.Image]}
                />
              </View>
              
              <View>
              <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                  <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Top Services</Text>
                <TouchableOpacity onPress={TopServices}>
                  <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                </TouchableOpacity>
              </View>
              <SliderB 
                  onPress={Service}
                  images={MainSlider[1]}
                  SliderImage={{width: wp('20%'),height:hp('10%')}}
                />
              </View>
              <View>
              <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                  <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Best Salons</Text>
                <TouchableOpacity onPress={Salons}>
                  <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                </TouchableOpacity>
              </View>
                <SliderC
                  images={MainSlider[2]}
                  SliderImage={{width: wp('46%'),height:hp('16%')}}
                />
              </View>
              <View>
              <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                  <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Popular Stylists</Text>
                <TouchableOpacity onPress={Stylists}>
                  <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                </TouchableOpacity>
              </View>
              <SliderC 
                  images={MainSlider[3]}
                  SliderImage={{width: wp('46%'),height:hp('16%')}}
                />
              </View>
              
              
        </ScrollView>
        </View>}
    </Container>
)
}

const styles = StyleSheet.create({
    SearchBar: {
    ...Inputs_Area.large,
    flex:1,
    padding:12
    },
    sectionStyle: {
        flexDirection: 'row',
        backgroundColor: '#d8d5d4ed',
        height: 40,
        borderRadius: 5,
        alignSelf:'center'
      },
      headermap:{
        flexDirection:"row",paddingTop:10,
        justifyContent:"space-between",
      },
      Image:{
        width:wp('95.4%'),
        height:hp('28%')
      }
    });
    const Lstyles = StyleSheet.create({
      SearchBar: {
      ...Inputs_Area.large,
      flex:1,
      padding:12
      },
      sectionStyle: {
          flexDirection: 'row',
          backgroundColor: '#d8d5d4ed',
          height: 40,
          borderRadius: 5,
          alignSelf:'center'
        },
        headermap:{
          flexDirection:"row",paddingTop:10,
          justifyContent:"space-between",
        },
        Image:{
          width:wp('78%'),
          height:hp('20%')
        }
      });
export default Home;
