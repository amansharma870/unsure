import React from "react";
import { Alert, Image, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Container from "../components/container";
import CustomButton from "../components/CustomButton";
import { Inputs_Area, Typography } from "../styles/Global";
import { ScreenName } from "../utils/Navigations/Routes";
import { utils } from "../utils/utils";

interface Props{
    route:any
    navigation:any
  }

const onPress=()=>{
    return(
        Alert.alert('✖ YOU ARE NOT A PREMIUM USER')
    )
}
const SignIn = (props:Props) =>{
    props.navigation.navigate(ScreenName.SIGNIN)
    }
export class SignUp extends React.Component{
    private EmailRef;PwdRef;
    
    render(){
        
        return(
            <ScrollView showsVerticalScrollIndicator={false}>
                <Container containerStyles={{marginTop:30}}>
                <StatusBar
                    animated={true}
                    backgroundColor="#0A5688"
                    
                    />
                <>
                <View>
                    <Text style={Typography.title}>Create Account</Text>
                </View>
                <View>
                    <TextInput returnKeyType={'next'}
                               onSubmitEditing={() => this.EmailRef.focus()} style={Inputs_Area.regular} placeholder={'Name'}/>
                    <TextInput returnKeyType={'next'}
                               ref={ref => this.EmailRef = ref}
                               onSubmitEditing={() => this.PwdRef.focus()} style={Inputs_Area.regular} placeholder={'Email'}/>
                    <TextInput ref={ref => this.PwdRef = ref} style={Inputs_Area.regular} placeholder={'Password'}/>
                    <Text style={Typography.non_regularText}>Gender</Text>
                </View>
                <View style={{flexDirection:'row',marginBottom:30}}>
                   <TouchableOpacity style={styles.gender}>
                      <Image style={{width:110,height:110}} source={utils.Img.SignUpA}/>  
                   </TouchableOpacity>
                   <TouchableOpacity style={styles.gender}>
                      <Image style={{width:110,height:110}} source={utils.Img.SignUpB}/>  
                   </TouchableOpacity>
                </View>
                <CustomButton textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyle} onPress={onPress} title={'Sign Up'}/>
                </>
            </Container>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    gender:{padding:12,
        borderWidth:1,
        borderRadius:12,
        borderColor:'#D8D8D8',
        marginTop:6,
        marginRight:15,
        marginLeft:15
    },
    buttonStyle:{
       backgroundColor:'#0A5688',
       borderRadius:12
    },
});

