
import React from 'react'
import { View,Image,StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { utils } from '../utils/utils'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context'

const PopularStylists=()=> {
        return (
                <SafeAreaView style={{backgroundColor:'#d8d5d4ed'}}> 
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems:'center'}}>
                        <Image style={styles.Main_Images} source={utils.Img.discountC}/>  
                    </View>
                    <View style={{alignItems:'center'}}>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.BarberA}/>  
                        <Image style={styles.Images} source={utils.Img.BarberB}/>  
                        <Image style={styles.Images} source={utils.Img.BarberC}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.BarberD}/>  
                        <Image style={styles.Images} source={utils.Img.BarberE}/>  
                        <Image style={styles.Images} source={utils.Img.BarberA}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.BarberB}/>  
                        <Image style={styles.Images} source={utils.Img.BarberC}/>  
                        <Image style={styles.Images} source={utils.Img.BarberD}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.BarberE}/>  
                        <Image style={styles.Images} source={utils.Img.BarberA}/> 
                        <Image style={styles.Images} source={utils.Img.BarberB}/>   
                    </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
export default PopularStylists

const styles = StyleSheet.create({
    Main_Images:{
        width: wp('93%'),
        height:hp('20%'),
        margin:hp('2%'),
       },
   Images:{
    width: wp('28%'),
    height:hp('14%'),
    margin:hp('1%')
   },

});