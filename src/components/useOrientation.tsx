import React from 'react'
import { Dimensions } from 'react-native';

export const useOrientation = ()=>{
const [orientation, setOrientation] = React.useState('portrait');
const isPortrait = () => {
const dim = Dimensions.get('screen');
return dim.height >= dim.width;
};
const isLandscape = () => {
const dim = Dimensions.get('screen');
return dim.width >= dim.height;
};
React.useEffect(()=>{
Dimensions.addEventListener('change', () => {
setOrientation(isPortrait() ? 'portrait' : 'landscape')
});
return () =>{
Dimensions.removeEventListener('change',isLandscape)
}
},[Dimensions])

// console.log(orientation)
return orientation;
}