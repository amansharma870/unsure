import React, { useState } from 'react'
import ContentContainer from '../components/ContentContainer'
import { utils } from '../utils/utils'
import MySalon from '../components/MySalon'
import { ScreenNames } from '../screens/Dashboard'
import { ActivityIndicator } from 'react-native'
interface Props{
    route:any
    navigation:any
  }

const Orange=(props:Props)=>{
    const [loading, setLoading] = useState(true);
    setTimeout(() => {
      setLoading(false);
    }, 100);
    const NearSalons = () =>{
        props.navigation.navigate(ScreenNames.BOOKAPPOINTMENT)
        }
        return (
            <ContentContainer>
                {loading ? (
          <ActivityIndicator
            color={'#0A5688'} size={55} style={{flex:1,}}
          />
        ) :
                <MySalon 
                   salonName={'Orange The Salon'}
                   service={'Hair Salon'}
                   star={'4.0'}
                   Rating={'(282 ratings)'}
                   ImageA={utils.Img.mySalonA}
                   ImageB={utils.Img.mySalonB}
                   ImageC={utils.Img.mySalonC}
                   ImageD={utils.Img.mySalonD}
                   ImageE={utils.Img.mySalonE}
                   Discription={'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'}
                   DaySecA={'Monday-Wednesday'}
                   DaySecB={'Friday-Sunday'}
                   TimeSecA={'10:00 AM - 11:00 PM'}
                   TimeSecB={'01:00 PM - 09:00 PM'}
                   address={'In front of South avenue,Epic road Darben'}
                   Distance={'2km'}
                   phone={9115915870}
                   BookAppointment={NearSalons}
                   /> }
            </ContentContainer>
        )
    }
    
     
    export default Orange