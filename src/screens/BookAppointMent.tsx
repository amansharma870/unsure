import React, { useState } from 'react'
import { View,ActivityIndicator,StyleSheet, Text, ListView, TouchableOpacity } from 'react-native'
import CalendarStrip from 'react-native-calendar-strip';
import CustomButton from '../components/CustomButton';
import { PrimaryTheme } from '../styles/Theme';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { ScrollView } from 'react-native-gesture-handler';
import ContentContainer from '../components/ContentContainer';
import { Typography } from '../styles/Global';
import { List } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import SliderC from '../components/ImageSliderC';
import Litem from '../components/LItem';
import { MainSlider } from '../styles/SliderData';
import { useOrientation } from '../components/useOrientation';
import { ScreenNames } from './Dashboard';
interface Props{
  route:any
  navigation:any
}
const BookAppointMent=(props:Props)=>{
  
  
  const CartScreen=()=>{
    props.navigation.navigate(ScreenNames.CARTSCREEN)
}



    const [loading, setLoading] = useState(true);

    const orientation = useOrientation();
    
    setTimeout(() => {
        setLoading(false);
      }, 1000);
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                {loading ? (
          <ActivityIndicator
            color={'#0A5688'} size={55}
          />
        ) :
                <View>
                    <CalendarStrip
                      scrollable
                      calendarAnimation={{type: 'sequence', duration: 30}}
                      daySelectionAnimation={{type: 'border', duration: 1, borderWidth: 1, borderHighlightColor: PrimaryTheme.$MY_Theme}}
                      style={[orientation === 'portrait' ? styles.calendarCss : Lstyles.calendarCss]}
                      calendarHeaderStyle={orientation === 'portrait' ? styles.calendarHeaderStyle : Lstyles.calendarHeaderStyle}
                      calendarColor={'transparent'}
                      dateNumberStyle={orientation === 'portrait' ? styles.dateNumberStyle : Lstyles.calendarHeaderStyle}
                      dateNameStyle={orientation === 'portrait' ? styles.dateNumberStyle : Lstyles.calendarHeaderStyle}
                      highlightDateNumberStyle={orientation === 'portrait' ? styles.dateNumberStyle : Lstyles.calendarHeaderStyle}
                      highlightDateNameStyle={orientation === 'portrait' ? styles.dateNumberStyle : Lstyles.calendarHeaderStyle}
                      iconContainer={{flex: 0.1}}
                    />
                   
                   <ContentContainer>
                   <ScrollView horizontal style={{borderBottomColor:'grey',borderBottomWidth:1,paddingBottom:0}} showsHorizontalScrollIndicator={false}>
                      <CustomButton onPress={{}} textStyle={{color:'#fff',fontSize:14}} buttonStyle={styles.buttonStyle} title={'10:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'11:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'12:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'01:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'02:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'03:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'04:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'05:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'06:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'07:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'08:00'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'09:00'}/>
                   </ScrollView>
                   <Text style={[Typography.subheading,{fontWeight:'bold',marginTop:hp('2%')}]}>
                     Services
                   </Text>
                   <ScrollView horizontal style={{paddingBottom:12}} showsHorizontalScrollIndicator={false}>
                      <CustomButton onPress={{}} textStyle={{color:'#fff',fontSize:14}} buttonStyle={styles.buttonStyle} title={'Hair'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'Face'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'Skin'}/>
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'Spa'}/>              
                      <CustomButton onPress={{}} textStyle={{color:'#0A5688',fontSize:13,fontWeight:'bold'}} buttonStyle={styles.buttonStyleB} title={'Hands & Feet'}/>    
                   </ScrollView>
                   
                   </ContentContainer>
                   <SafeAreaView>
                        <ContentContainer containerStyles={{marginHorizontal:10,paddingBottom:0}}>
                          <List.Accordion 
                             title={<View style={{}}><Text style={{}}>Haircut</Text></View>} id="1" 
                             style={{padding:0}}
                             theme={{ colors: { primary: '#000' }}}>
                                 <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Trim'} Distance={'10 m'} Price={'₹ 200'}/>
                                     </>} />
                                     <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Haircut - Stylist'} Distance={'30 m'} Price={'₹ 400'}/>
                                     </>} />
                                          </List.Accordion>
                                          <List.Accordion 
                             title={<View style={{}}><Text style={{}}>Hair Care</Text></View>} id="1" 
                             style={{padding:0}}
                             theme={{ colors: { primary: '#000' }}}>
                                 <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Trim'} Distance={'10 m'} Price={'₹ 200'}/>
                                     </>} />
                                     <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Haircut - Stylist'} Distance={'30 m'} Price={'₹ 400'}/>
                                     </>} />
                                          </List.Accordion>
                                          <List.Accordion 
                             title={<View style={{}}><Text style={{}}>Hair Color</Text></View>} id="1" 
                             style={{padding:0}}
                             theme={{ colors: { primary: '#000' }}}>
                                 <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Trim'} Distance={'10 m'} Price={'₹ 200'}/>
                                     </>} />
                                     <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Haircut - Stylist'} Distance={'30 m'} Price={'₹ 400'}/>
                                     </>} />
                                          </List.Accordion>
                                          <List.Accordion 
                             title={<View style={{}}><Text style={{}}>Hair Styling</Text></View>} id="1" 
                             style={{padding:0,marginBottom:20}}
                             theme={{ colors: { primary: '#000' }}}>
                                 <List.Item
                                 style={{padding:0}}
                                     title={<>
                                     <Litem  Service={'Trim'} Distance={'10 m'} Price={'₹ 200'}/>
                                     </>} />
                                     <List.Item
                                     
                                 style={{padding:0,marginBottom:12}}
                                     title={<>
                                     <Litem  Service={'Haircut - Stylist'} Distance={'30 m'} Price={'₹ 400'}/>
                                     </>} />
                                          </List.Accordion>
                        <ContentContainer containerStyles={{marginHorizontal:6}}>
                          <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                              <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Popular Stylists</Text>
                            <TouchableOpacity>
                              <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                            </TouchableOpacity>
                          </View>
                          <SliderC 
                              images={MainSlider[3]}
                              SliderImage={{width: wp('44%'),height:hp('15%')}}
                            />
                            <CustomButton onPress={CartScreen} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Book  Appointment'}/>
                        </ContentContainer>
                        </ContentContainer>
                   </SafeAreaView>
                </View>
                }
            </ScrollView>
            
        )
    }
export default BookAppointMent
const styles = StyleSheet.create({
  buttonStyle:{
    backgroundColor: PrimaryTheme.$MY_Theme,
    borderWidth:0,
    borderColor:'#fff',width:wp('15%'),marginRight:5,height:hp('5%'),
  },
  buttonStyleB:{
      backgroundColor: 'transparent',
      borderWidth:0,
      borderColor:PrimaryTheme.$MY_Theme,width:wp('22%'),marginRight:0,height:hp('5%')
 
    },
listItemGap:{
   flexDirection:'row',
},

container1: {
  flexDirection: 'row',
  marginBottom:hp('1%'),
},
redbox: {
  width: wp('35%'),
},
bluebox: {
  width: wp('32%'),
},
blackbox: {
width: wp('20%'),
alignItems: 'flex-end'
},
buttonStyleC:{
  backgroundColor: PrimaryTheme.$MY_Theme,
  borderWidth:0,
  borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%'),
  marginTop:4
},
calendarCss:{
  height: 100, paddingTop: 10
},
calendarHeaderStyle:{
  color:PrimaryTheme.$MY_Theme,
  fontSize:16
},
dateNumberStyle:{
  color:PrimaryTheme.$MY_Theme,
},
onDateSelect:{
  
}
});
const Lstyles = StyleSheet.create({

calendarCss:{
  height: 130, paddingTop: 20,paddingBottom:20
},
calendarHeaderStyle:{
  color:PrimaryTheme.$MY_Theme,
  fontSize:16,
},
onDateSelect:{
  
}
});