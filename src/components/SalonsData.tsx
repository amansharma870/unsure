import React from 'react'
import { Text, View,Image,StyleSheet,ImageStyle,TouchableOpacity } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from './Icon';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import { useOrientation } from './useOrientation';

interface Props{
    style?:
    |   ImageStyle
    |   ImageStyle[],
    Name?:any,
    Rating?:any,
    Venue?:any,
    Distance?:any,
    Price?:string,
    IconName?:any,IconSize?:any,IconColor?:any,IconStyle?:any,
    Category?:any,
    SalonImage:any,
    onPress:any
}
const SalonsData=(props:Props)=> {
    const orientation = useOrientation();
        return (             
                <TouchableOpacity onPress={props.onPress} activeOpacity={0.9} style={{width:'100%'}}>
                <TouchableOpacity onPress={props.onPress}  activeOpacity={0.9}>
                    <Image 
                        source={props.SalonImage}
                        style={[orientation === 'portrait' ? styles.Image : Lstyles.Image]}
                    />
                    </TouchableOpacity>
                    <View style={{flexDirection:"row",justifyContent:'space-between'}}>
                            <Text style={[Typography.BottomTitle,{color:'#000'}]}>{props.Name}</Text>
                       <View style={{flexDirection:"row",justifyContent:'space-between'}}>
                       <Icon name={'star'} size={16}
                          color={PrimaryTheme.$DARK_COLOR}
                      />  
                       <Text style={[Typography.heading,{color:'#000',fontSize:12.3}]}>{props.Rating}</Text>
                       </View>
                    </View>
                    <View style={{flexDirection:"row",justifyContent:'space-between'}}>
                       <Text style={[Typography.subheading,{color:PrimaryTheme.$DARK_COLOR,fontSize:12.3}]}>{props.Venue}</Text>
                     
                       <Text style={[Typography.subheading,{color:PrimaryTheme.$DARK_COLOR,fontSize:12.3}]}>{props.Distance}</Text>
                    </View>
                    <View style={{flexDirection:"row"}}>
                       <Text style={[Typography.subheading,{color:PrimaryTheme.$MY_Theme,fontSize:14,fontWeight:'bold'}]}>{props.Price}</Text>
                     
                       <Text style={[Typography.subheading,{color:PrimaryTheme.$DARK_COLOR,fontSize:14}]}>{props.Category}</Text>
                    </View>
                    </TouchableOpacity>
                    
        )
    }

    const styles = StyleSheet.create({
          Image:{
            width:'100%',height:hp('25%'),marginTop:7,marginBottom:7
           },
        });
        const Lstyles = StyleSheet.create({
            Image:{
              width:'100%',height:hp('30%'),marginTop:7,marginBottom:7,borderRadius:5
             },
          });
export default SalonsData

