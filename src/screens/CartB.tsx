import React, { useState } from "react";
import { View, StyleSheet, Image,Text } from "react-native";
import { Switch } from 'react-native-paper';
import { ScrollView } from "react-native-gesture-handler";
import Container from "../components/container";
import { PrimaryTheme } from "../styles/Theme";
import { heightPercentageToDP } from "react-native-responsive-screen";
import CustomButton from "../components/CustomButton";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { utils } from "../utils/utils";
import { ScreenNames } from "./Dashboard";
interface Props{
  route:any
  navigation:any
}
const CardB = (props:Props) => {
  const CBooking=()=>{
    props.navigation.navigate(ScreenNames.CBOOKINGS)
}
  const [isSwitchOn, setIsSwitchOn] = React.useState(false);
  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

 
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
       <Container>
         <View style={styles.sectionStyle}>
         <View style={{padding:18}}>
             <Image source={utils.Img.payments_logo}/>
           </View>

           <View style={{marginTop:heightPercentageToDP('15%'),flexDirection:'row',justifyContent:'space-between'}}>
             <Text>Save card information</Text>
              <Switch color={PrimaryTheme.$MY_Theme} focusable={true} value={isSwitchOn} onValueChange={onToggleSwitch} />
           </View>
         </View>
         <CustomButton onPress={CBooking} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Pay ₹ 600'}/>
       </Container>
    </ScrollView>
  );
};
 
const styles = StyleSheet.create({

  buttonStyleC:{
    backgroundColor: PrimaryTheme.$MY_Theme,
    borderWidth:0,
    borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%'),
    marginTop:hp('3%')
  },
  sectionStyle: {
    marginTop:hp('1%')
   },
  });


export default CardB;