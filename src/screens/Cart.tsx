import * as React from 'react';
import {ImageBackground, Text, View,StyleSheet,Image, Switch, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../components/container';
import { Typography } from '../styles/Global';
import Cartdata from '../components/CartData';
import { utils } from '../utils/utils';
import { PrimaryTheme } from '../styles/Theme';
import CustomButton from '../components/CustomButton';
import { ScreenNames } from './Dashboard';

interface Props{
  route:any
  navigation:any
}
const Cart = (props:Props) =>{
  const Payment=()=>{
    props.navigation.navigate(ScreenNames.CARDSCREEN_B)
}
    return(
      <Container containerStyles={{justifyContent:"flex-start",marginTop:5,marginHorizontal:15}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{marginBottom:10}}>
          <Text style={[Typography.heading,{fontSize:20}]}>
            Orange The Salon
          </Text>
          </View>
         <View style={{borderBottomWidth:1,marginBottom:20}}>
            <Cartdata 
              title='Haircut - Stylist' 
              time='10:00AM'
              num='₹'
              num1='400'
              img={utils.Img.Profile}
            />
         </View>
                <View style={{borderBottomWidth:1,marginBottom:20}}>
                    <Cartdata 
                      title={'Hair Spa -\nShoulder Length' }
                      time='10:00AM'
                      num='₹'
                      num1='400'
                    img={utils.Img.Profile}/>
                </View>
              <View style={styles.sectionStyle}>
                  <TextInput 
                  placeholder={'Enter Coupon code'}
                  />
                  <Text style={{color:PrimaryTheme.$MY_Theme,fontWeight:"bold",padding:12}}>Apply</Text>
              </View>
          <View style={{borderBottomWidth:1,marginTop:100,marginBottom:10}}>
          </View>

                <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
                                <Text style={[Typography.subheading,{fontWeight:'bold'}]}>Total Amount</Text>
                                <TouchableOpacity>
                                <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold',paddingRight:12}}>₹ 1200</Text>
                                </TouchableOpacity>
                            </View> 
                            <CustomButton onPress={Payment} textStyle={{color:'#fff'}} buttonStyle={styles.ButtonSingin} title={'Booking confirm'}/>
                </ScrollView>
      </Container>
   
    )
}


export default Cart;


const styles = StyleSheet.create({
  ButtonSingin:{

    backgroundColor:PrimaryTheme.$MY_Theme,
    marginBottom: hp('1%'),
   color:PrimaryTheme.$MY_Theme,
   borderRadius: 10,
  },
  sectionStyle: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderColor: '#000',
    justifyContent:"space-between",
  },
  Textcolor:{

    color:PrimaryTheme.$MY_Theme,
    fontWeight:"bold"
  }
});
