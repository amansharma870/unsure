import React, { useState } from "react";
import { View, StyleSheet, Image,Text, TextInput, Alert, Switch } from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Container from "../components/container";
import { PrimaryTheme } from "../styles/Theme";
import { heightPercentageToDP } from "react-native-responsive-screen";
import CustomButton from "../components/CustomButton";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import ImagePicker from 'react-native-image-crop-picker';
import { OptionsButton } from 'react-native-options-button'
import ContentContainer from "../components/ContentContainer";
import { SafeAreaView } from "react-native-safe-area-context";
import { Typography } from "../styles/Global";
import Icon from "../components/Icon";
import { ScreenNames } from "./Dashboard";
interface Props{
  route:any
  navigation:any
}
const Profile = (props:Props) => {
  const ChangePWD=()=>{
    props.navigation.navigate(ScreenNames.CPWD)
}

const [image,setImage]=useState('../assets/Images/1.png');


const GetfromCamera =()=>{
  ImagePicker.openCamera({
    width: 300,
    height: 300,
    cropping: true,
  }).then(image => {
    setImage(image.path)
  }).catch((err) => { console.log("openCamera catch" + err.toString()) });
}
const GetfromLibrary =()=>{
  ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true
  }).then(image => {
    console.log(image);
    setImage(image.path)
  }).catch((err) => { console.log("openCamera catch" + err.toString()) });
}
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
       <ContentContainer containerStyles={{paddingTop:12}}>
      

        <SafeAreaView>
        <SafeAreaView style={{flexDirection:"row"}}>
            <View
              style={{
                width:wp('38%')
              }}>
              <Image style={{backgroundColor:PrimaryTheme.$MY_Theme,borderRadius:100,borderWidth:0,borderColor:PrimaryTheme.$MY_Theme,marginLeft:wp('2%'),marginBottom:wp('2%')}} height={hp('18%')} width={wp('32%')} source={{uri:image}}/>  
              <CustomButton onPress={GetfromCamera} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Use Camera'}/>
                <CustomButton onPress={GetfromLibrary} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Gallary'}/>
                
            </View>
            <View
              style={{
                width:wp('55%')
              }}>
              <View style={{flexDirection:'row'}}>
                <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MY_Theme}]}>Name: </Text><Text style={[Typography.heading,{fontSize:18}]}>Aman Sharma</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MY_Theme}]}>City: </Text><Text style={[Typography.heading,{fontSize:18}]}>Abohar</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MY_Theme}]}>Contact: </Text><Text style={[Typography.heading,{fontSize:18}]}>9115915870</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MY_Theme}]}>Gmail: </Text><Text style={[Typography.heading,{fontSize:18}]}>amansharma@gmail.com</Text>
              </View>
            </View>
            
        </SafeAreaView>
           <SafeAreaView>
           <View style={{paddingTop:hp('5%')}}>
              <Text style={[Typography.heading,{fontSize:22}]}>Account Settings</Text>
            </View>
            <TouchableOpacity onPress={ChangePWD} style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={[Typography.subheading,{fontSize:18}]}>Change Password</Text>
              <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                      color={'#686868'}
                /> 
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={[Typography.subheading,{fontSize:18}]}>Notifications</Text>
              <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                      color={'#686868'}
                /> 
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={[Typography.subheading,{fontSize:18}]}>Payment Options</Text>
              <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                      color={'#686868'}
                /> 
            </TouchableOpacity>
           </SafeAreaView>
           <SafeAreaView>
           
            
            <TouchableOpacity style={{flexDirection:'row',paddingTop:hp('20%')}}>
            <Icon  name={'log-out-outline'} size={28} style={{paddingRight:wp('1%')}}
                      color={PrimaryTheme.$MY_Theme}
                /> 
              <Text style={[Typography.subheading,{fontSize:18,color:PrimaryTheme.$MY_Theme}]}>Sign Out</Text>
            </TouchableOpacity>
           </SafeAreaView>
        </SafeAreaView>
       </ContentContainer>
    </ScrollView>
  );
};
 
const styles = StyleSheet.create({

  buttonStyleC:{
    backgroundColor: PrimaryTheme.$MY_Theme,
    borderWidth:2,
    borderColor:PrimaryTheme.$MY_Theme,width:'90%',height:hp('5%'),
  },
  sectionStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MY_Theme
   },
   sectionStyleTop: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MY_Theme
   },
searchIcon: {
    padding: 10,
},
input: {
    paddingRight: 10,
    paddingLeft: 0,
    color: '#424242',
},
  });


export default Profile;