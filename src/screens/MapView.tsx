import * as React from 'react';
import {StyleSheet,View} from 'react-native';
import MapView from 'react-native-maps';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Map = () =>{

return(
    <View>
        <View style={styles.container}>
                <MapView
            style={styles.map}
            initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            }}
            />
        </View>
    </View>
)
}

const styles = StyleSheet.create({
container: {
...StyleSheet.absoluteFillObject,
height: hp('100%'),
width:wp('100%'),
justifyContent: 'flex-end',
alignItems: 'center',

},
map: {
...StyleSheet.absoluteFillObject,
},
});
export default Map;