export enum PrimaryTheme {
  $DARK_COLOR = '#000',
  $DARK_PRIMARY_COLOR = '#D32F2F',
  $LIGHT_PRIMARY_COLOR = '#FFCDD2',
  $Red_COLOR = '#FF5252',
  $TEXT_ICON_COLOR = '#FFFFFF',
  $ACCENT_COLOR = '#03A9F4',
  $TEXT_COLOR_900 = '#000000',
  $TEXT_COLOR_500 = '#999999',
  $TEXT_COLOR_300 = '#666666',
  $TEXT_COLOR_700 = '#333333',
  $DIVIDER_COLOR = '#BDBDBD',
  $BACK_GRIOND = '#79b958',
  $BUTTON_COLOR = '#4cb051',
  $button_White = '#fff',
  $Error_Color = 'red',
  $MY_Theme = '#0A5688',

}