import React, { useState } from 'react'
import ContentContainer from '../components/ContentContainer'
import { utils } from '../utils/utils'
import MySalon from '../components/MySalon'
import { ScreenNames } from '../screens/Dashboard'
import { ActivityIndicator,Text, View } from 'react-native'
interface Props{
    route:any
    navigation:any
    ReqName:any
  }

const Choco=(props:Props)=>{
    const [loading, setLoading] = useState(true);
    setTimeout(() => {
      setLoading(false);
    }, 100);
    const NearSalons = () =>{
        props.navigation.navigate(ScreenNames.BOOKAPPOINTMENT)
        }
        return (
            <ContentContainer>
                {loading ? (
          <ActivityIndicator
            color={'#0A5688'} size={55} style={{flex:1,}}
          />
        ) :
                <MySalon 
                   salonName={'Choco Style You'}
                   service={'Hair Salon'}
                   star={'4.0'}
                   Rating={'(328 ratings)'}
                   ImageA={utils.Img.ChocoSalonA}
                   ImageB={utils.Img.ChocoSalonB}
                   ImageC={utils.Img.ChocoSalonC}
                   ImageD={utils.Img.ChocoSalonD}
                   ImageE={utils.Img.ChocoSalonE}
                   Discription={'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'}
                   DaySecA={'Monday-Wednesday'}
                   DaySecB={'Friday-Sunday'}
                   TimeSecA={'10:00 AM - 11:00 PM'}
                   TimeSecB={'01:00 PM - 09:00 PM'}
                   address={'In front of South avenue,Epic road Darben'}
                   Distance={'2.5km'}
                   phone={9115915870}
                   BookAppointment={NearSalons}
                   />
                }
            </ContentContainer>
        )
    }
    
     
    export default Choco