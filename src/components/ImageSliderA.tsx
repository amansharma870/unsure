import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, FlatList} from "react-native"
import ImageOverlay from "react-native-image-overlay";
import { Typography } from '../styles/Global';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useOrientation } from './useOrientation';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'


interface Props{
    onPress?: any;
    subPlace?: any;
    subHeading?: any;
    overlayAlpha?: any;
    overlayColor?: any;
    contentContainerCustomStyle?:any
        containerStyle?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
    SliderImage:any,
    images:any
}

const Slider = (props:Props) => {
    const orientation = useOrientation();



    return (
        <View style={{marginTop:13,flexDirection: 'row', 
        flexWrap: 'wrap',marginBottom:12}}>

<FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.images}
        renderItem={({ item, index }) => (
            <TouchableOpacity style={{flexDirection: 'row', 
            flexWrap: 'wrap',paddingRight:wp('3%')}} onPress={props.onPress}activeOpacity={0.9} key={index}>

                <ImageOverlay
                
                source={item.image}
                containerStyle={[styles.image,props.SliderImage]}
                overlayColor={props.overlayColor}
                overlayAlpha={props.overlayAlpha}
                >
                   <View>
                        <Text style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>{item.title}</Text>
                        <Text style={[Typography.paragraph,{color:"#fff",fontSize: 20,fontWeight:'bold'}]}>{item.subTitle}</Text>
                        <Text style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>{item.Paragraph}</Text>
                    </View>
                </ImageOverlay>
                </TouchableOpacity>
        )}
        keyExtractor={(item,index) => index.toString()}

      />
    </View>
    )
}
export default Slider

const styles = StyleSheet.create({
container: {
// backgroundColor: '#fff',
// borderRadius: 18,
// width: wp('80%'),
// height: hp('25%'),
},
image: {
borderRadius: 5,
},

})