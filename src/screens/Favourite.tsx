import * as React from 'react';
import { Text, View,Image,StyleSheet, TouchableOpacity ,ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Container from '../components/container';
import CustomButton from '../components/CustomButton';
import FData from '../components/FavData';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';


const Favourite = ()=>{
    return (
        <Container containerStyles={{justifyContent:"flex-start",marginTop:20,marginHorizontal:15,alignItems:'stretch'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{flexDirection:"row",marginBottom:10}}>
                <CustomButton onPress={{}} textStyle={{color:'#fff'}} buttonStyle={styles.ButtonTime} title={'Salons'}/>
                    <TouchableOpacity style={{marginTop:10}}>
                    <Text style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$MY_Theme}]}>
                             Stylists</Text>
                    </TouchableOpacity>
                </View>
                <FData />
                <FData />
                <FData />
                <FData />
                <FData />
            </ScrollView>
            </Container>
    )
}
const styles = StyleSheet.create({
    ButtonTime:{
      backgroundColor:PrimaryTheme.$MY_Theme,
      width:wp('30%'),
      borderRadius: 10,
      height: hp('6.5%'),
      fontSize:14,
      letterSpacing:1, 
      marginRight:10
  },
    })
export default Favourite;