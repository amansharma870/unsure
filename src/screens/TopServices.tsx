import React from 'react'
import { View,Image,StyleSheet, TouchableOpacity } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { utils } from '../utils/utils'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context'
interface Props{
    route:any
    navigation:any
  }
const TopServices=(props:Props)=> {

        return (
                <SafeAreaView style={{backgroundColor:'#d8d5d4ed'}}> 
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems:'center'}}>
                        <Image style={styles.Main_Images} source={utils.Img.discountA}/>  
                    </View>
                    <View style={{alignItems:'center'}}>
                    <TouchableOpacity  style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.CrausalA1}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA2}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA3}/>  
                    </TouchableOpacity>
                    <TouchableOpacity  style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.CrausalA3}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA4}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA1}/>  
                    </TouchableOpacity>
                    <TouchableOpacity  style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.CrausalA1}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA2}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA3}/>  
                    </TouchableOpacity>
                    <TouchableOpacity  style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.CrausalA3}/>  
                        <Image style={styles.Images} source={utils.Img.CrausalA4}/> 
                        <Image style={styles.Images} source={utils.Img.CrausalA1}/>   
                    </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
export default TopServices

const styles = StyleSheet.create({
    Main_Images:{
        width: wp('93%'),
        height:hp('20%'),
        margin:hp('2%'),
       },
   Images:{
    width: wp('28%'),
    height:hp('14%'),
    margin:hp('1%')
   },

});