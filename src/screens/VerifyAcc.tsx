import React from "react";
import { Image, ActivityIndicator, StatusBar, StyleSheet, Text, View, ScrollView } from "react-native";
import Container from "../components/container";
import CustomButton from "../components/CustomButton";
import { Div_Area, Typography } from "../styles/Global";
import { TextInput } from "react-native-gesture-handler";
import { ScreenName } from "../utils/Navigations/Routes";
import { utils } from "../utils/utils";
import { useOrientation } from "../components/useOrientation";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
  route:any
  navigation:any
}
const Verify=(props:Props)=>{
  const orientation = useOrientation();

  const Dashboard = () =>{
    props.navigation.navigate(ScreenName.DASHBOARD)
    }

    return(
        <ScrollView showsVerticalScrollIndicator={false}>
          <Container containerStyles={[orientation === 'portrait' ? styles.mystyle : Lstyles.mystyle]}>
            <StatusBar
            animated={true}
            backgroundColor="#0A5688"
            />
            <View style={[orientation === 'portrait' ? styles.atall : Lstyles.atall]}>
              <View style={{alignItems:'center'}}>
                  <Text style={Typography.title}>Verify Account</Text>
              </View>
              <View style={Div_Area.title_Image}>
                <Image style={Div_Area.title_Image} source={utils.Img.Verify}/>  
              </View>
            </View>
            <View style={Div_Area.regular}>
                <Text style={Typography.subheading}>Enter four digit code sent on your mobile number.</Text>
                <View style={{flexDirection:"row",alignSelf:'center',margin:12}}>
                    <TextInput style={{backgroundColor:'#fff',fontWeight:'600',borderWidth:1,borderRadius:8,width:45,marginRight:19}}/>
                    <TextInput style={{backgroundColor:'#fff',fontWeight:'600',borderWidth:1,borderRadius:8,width:45,marginRight:19}}/>
                    <TextInput style={{backgroundColor:'#fff',fontWeight:'600',borderWidth:1,borderRadius:8,width:45,marginRight:19}}/>
                    <TextInput style={{backgroundColor:'#fff',fontWeight:'600',borderWidth:1,borderRadius:8,width:45,marginRight:19}}/>
                </View>
                
                <CustomButton onPress={Dashboard} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleA} title={'Verify OTP'}/>
                <View style={{flexDirection:'row',alignSelf:'center'}}>
                  <Text>Did not receive OTP?</Text>
                  <Text style={{fontWeight:'bold',color:'#0A5688',marginLeft:5}}>Resend OTP</Text>
                </View>
            </View>
        </Container>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    buttonStyleB:{
        backgroundColor: 'transparent',
        borderWidth:1,
        borderColor:'#909090'
      },
      buttonStyleA:{
        backgroundColor: '#0A5688',
        borderWidth:1,
        borderColor:'#909090',
        marginTop:12
      },
      borderStyleBase: {
        width: 30,
        height: 45
      },
     
      borderStyleHighLighted: {
        borderColor: "#03DAC6",
      },
     
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
      },
      underlineStyleHighLighted: {
        borderColor: "#03DAC6",
      },
      mystyle:{
        marginTop:30
      },
      atall:{
        padding:0
    }
});
const Lstyles = StyleSheet.create({

    mystyle:{
      flexDirection:'row',
      height:hp('53%')
  },
  atall:{
      padding:20
  }
});
export default Verify