import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, Image, FlatList} from "react-native"
import { Typography } from '../styles/Global';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
    onPress:any;
    subPlace?: any;
    subHeading?: any;
    overlayAlpha?: any;
    overlayColor?: any;
    contentContainerCustomStyle?:any;
    containerStyle?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
    SliderImage:any,
    images:any
}

const SliderB = (props:Props) => {



    return (
    <View style={{marginTop:13,flexDirection: 'row', 
    flexWrap: 'wrap'}}>
        <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.images}
        renderItem={({ item, index }) => (
            <TouchableOpacity  onPress={props.onPress}activeOpacity={0.9} key={index}>

                <Image
                source={item.image}
                    style={[styles.image,props.SliderImage]}
                />
                   <View style={{alignItems:'center',paddingRight:wp('6.8%'),margin:wp('3%')}}>
                        <Text style={Typography.BottomTitle}>{item.subTitle}</Text>
                        <Text style={Typography.BottomSubTitle}>{item.subPoint}</Text>
                    </View>
                </TouchableOpacity>
        )}
        keyExtractor={(item,index) => index.toString()}
      />

    </View>
    )
}
export default SliderB

const styles = StyleSheet.create({
container: {
// backgroundColor: '#fff',
// borderRadius: 18,
// width: wp('80%'),
// height: hp('25%'),
},
image: {
borderRadius: 5,
},
})