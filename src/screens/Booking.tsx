import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground, Dimensions, CheckBox, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import BData from '../components/BookingData';
import Container from '../components/container';
import CustomButton from '../components/CustomButton';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import { utils } from '../utils/utils';


interface Props {
 id?:any,
 index?:any,
 route:any,
 navigation:any
}
const BookingScreen = (props:Props) => {
 
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{backgroundColor:"#b0ddf0",}}>
          <View style={{flexDirection:"row",marginHorizontal:15,marginTop:10,marginBottom:10}}>
              <View style={{marginRight:10}}>
                <Image style={{width:wp('20%'),height:hp('12%')}} resizeMode={'contain'} source={utils.Img.Profile} 
                />
              </View>
          <View>
            <Text style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$MY_Theme,marginBottom:5}]}>Margaret Oliver</Text>
            <Text style={[Typography.subheading,{fontSize:wp('3.8%'),color:PrimaryTheme.$MY_Theme,marginBottom:5}]}>marget.oliver@gmail.com</Text>
          <Text style={[]}>123 456 7890</Text>
          </View>
          </View>
          </View>
      <Container containerStyles={{justifyContent:"flex-start",marginHorizontal:15,
          marginTop:24,alignItems:"stretch"}}>  
                <View style={{flexDirection:"row",marginBottom:12}}>
                  <CustomButton onPress={{}} textStyle={{color:'#fff'}} buttonStyle={styles.ButtonTime} title={'Upcoming'}/>
                  <TouchableOpacity style={{marginTop:10}}>
                  <Text style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$MY_Theme}]}>Previous</Text>
                  </TouchableOpacity>
                </View>
         <BData 
        img={utils.Img.SalonA}
        title='Orange The Salon'
        title1='Haircut'
        title2='Hair Spa'
        time='Sept 19 10:00 AM'
         />
           <BData 
        img={utils.Img.SalonB}
        title='Enhance Salon'
        title1='Oil Massage'
        time='Sept 21 10:00 AM'
         />
      </Container>
      </ScrollView>
    );
  }; 
const styles = StyleSheet.create({
  ButtonTime:{
    backgroundColor:PrimaryTheme.$MY_Theme,
    width:wp('30%'),
    borderRadius: 10,
    height: hp('6.5%'),
    fontSize:14,
    padding:9,
    letterSpacing:1, 
    marginRight:10
},
  })
export default BookingScreen;
