import React from 'react';
import { View ,Image,Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import Icon from './Icon';

interface Props{
  img?:any,
  title?:any;
  time?:any;
  num?:any,
  num1?:any
}
const Cartdata = (props:Props) => {
    return(
        <>
        <View style={{flexDirection:"row",marginBottom:9}}>
               <View style={{width:wp('20%')}}>
                  <Image style={{width:wp('15%'),height:hp('5%')}} resizeMode={'contain'} source={props.img} />
               </View>
               <View style={{width:wp('49%')}}>
                <Text style={{fontSize:12}}>{props.title}</Text>
                <Text>{props.num} <Text style={{color:PrimaryTheme.$MY_Theme,fontWeight:"bold"}}>{props.num1}</Text></Text>
               </View>
               <View style={{width:wp('23%'),}}>
               <View style={{flexDirection:'row'}}>
                 <Text style={{paddingRight:4}}>
                 {props.time}
                 </Text>
                          <Icon name={'chevron-down-outline'} size={20}
                            color={PrimaryTheme.$MY_Theme}
                          />  
               </View>
              <View style={{alignItems:"flex-end"}}>
                          <Icon name={'trash-outline'} size={25} style={{paddingTop:12}}
                            color={PrimaryTheme.$MY_Theme}
                          />
                </View>  
               </View>
           </View>
          
        </>
    )
}

export default Cartdata;

// Utils.image.scissor