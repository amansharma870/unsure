import * as React from 'react';
import {SafeAreaView, StyleSheet, View, ViewStyle,Text, TouchableOpacity,} from 'react-native';
import { PrimaryTheme } from '../styles/Theme';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import Icon from './Icon';
import { useState } from 'react';

interface Props {
  Service: any;
  Distance:any;
  Price:any;
  containerStyles?: ViewStyle | ViewStyle[];
}
const Litem = (props:Props) => {
    const [toggleCheckBox, setToggleCheckBox] = useState(false)

  return (
    <SafeAreaView>
      
                                                  <View style={{}}>
                                        <View style = {styles.container1}>
                                          <View style = {styles.redbox} >
                                            <Text>{props.Service}{'\n'}{props.Distance}</Text>
                                          </View>
                                          <View style = {styles.bluebox} >
                                            <Text style={{ textAlign: 'right', }}>{props.Price}</Text>
                                          </View>
                                            <View style = {styles.blackbox} >
                                            
                                            </View>
                                        </View>
                                            </View>
                                          
                                              
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    buttonStyle:{
      backgroundColor: PrimaryTheme.$MY_Theme,
      borderWidth:0,
      borderColor:'#fff',width:wp('15%'),marginRight:5,height:hp('5%'),
    },
    buttonStyleB:{
        backgroundColor: 'transparent',
        borderWidth:0,
        borderColor:PrimaryTheme.$MY_Theme,width:wp('22%'),marginRight:0,height:hp('5%')
   
      },
  listItemGap:{
     flexDirection:'row',
  },
  
  container1: {
    flexDirection: 'row',
  },
  redbox: {
    width: wp('65%'),
  },
  bluebox: {
    width: wp('15%'),
    alignItems: 'flex-end',
    paddingTop:7
  },
  blackbox: {
  width: wp('12%'),
  alignItems: 'flex-end',
  },
  checkbox:{
  
  }
  });

export default Litem;