import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, Image, TouchableOpacity, FlatList} from "react-native"
import Icon from './Icon';
import { Typography } from '../styles/Global';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
    onPress?:any;
    subPlace?: any;
    subHeading?: any;
    overlayAlpha?: any;
    overlayColor?: any;
    contentContainerCustomStyle?:any;
    containerStyle?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
    SliderImage:any,
    images:any
}

const ImageSliderC = (props:Props) => {
const isCarousel = React.useRef(null)



    return (
    <View style={{marginTop:8,marginBottom:8,marginLeft:0, 
    flexDirection: 'row', 
    flexWrap: 'wrap'}}>

<FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.images}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={props.onPress}activeOpacity={0.8} key={index}>
          <Image
          source={item.image}
          style={[styles.image,props.SliderImage]}
          />
       
       
          <View  style={{flexDirection:"row",marginTop:5,paddingRight:wp('10%')}}>
          <Text style={[Typography.BottomName,{fontSize:12, fontWeight:'900',color:"#0A5688",
            paddingRight:wp('14%'),}]}>{item.Fname}
          </Text>
            <View style={{flexDirection:"row",marginLeft: 10,}}>
              <Icon name={'star'} size={15} style={{marginRight:2,}}
                  color={'#0A5688'}
              />  
              <Text style={[Typography.BottomName,{fontSize:12,
                fontWeight:'900',color:'#0A5688'}]}>{item.star}
              </Text>
            </View>
          </View>
            <Text style={[Typography.BottomName,{fontSize:12,
                fontWeight:'900',color:'#0A5688'}]}>{item.place}
            </Text>
        </TouchableOpacity>
        )}
        keyExtractor={(item,index) => index.toString()}
      />
    </View>
    )
}
export default ImageSliderC

const styles = StyleSheet.create({
container: {
// backgroundColor: '#fff',
// borderRadius: 18,
// width: wp('80%'),
// height: hp('25%'),
},
image: {
borderRadius: 0,
},
})