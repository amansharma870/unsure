import React from 'react';
import { View ,Image,Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import { utils } from '../utils/utils';
import Icon from './Icon';

interface Props{
    img?:any,
    title?:any,
    title1?:any,
    title2?:any,
    time?:any
}
const BData = (props:Props)=>{
    return(
        <>
        <View style={{flexDirection:"row",marginBottom:12}}>
           <View style={{width: wp('32%'),}}>
           <Image style={{width:wp('30%'),height:hp('13%')}} 
                    resizeMode={'contain'} source={utils.Img.salonPlaceB} 
                    />
           </View>
           <View style={{width: wp('36%'),}}>
           <Text style={[Typography.heading,{fontSize:wp('3.7%'),color:PrimaryTheme.$MY_Theme,marginBottom:5}]}>{props.title}</Text>
           <View style={{marginBottom:0}}>
           <Text style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$MY_Theme,}]}>{props.title1} 
           <Text style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$MY_Theme,}]}> {props.title2} </Text>
           </Text>
           </View>
               <Text style={[Typography.subheading,{fontSize:14}]}>{props.time}</Text>
           </View>
           <View style={{width: wp('22%'),alignItems:"flex-end"}}>
             <View style={{flexDirection:"row"}}>
              <Icon name={'star'} size={14} style={{}}
                  color={PrimaryTheme.$MY_Theme}
              /> 
              <Text> 4.5</Text>
              </View>
               
              <View style={{marginTop:40,flexDirection:"row"}}>
              <Icon name={'call'} size={20} style={{marginRight:10,}}
                  color={PrimaryTheme.$MY_Theme}
              /> 
                <Icon name={'close'} size={20} style={{backgroundColor: PrimaryTheme.$MY_Theme,borderRadius: 10,}}
                  color='#FFF'
              /> 
               
              </View>
              
             </View>
         
         </View>
        </>
    )
}

export default BData;
