import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
    FeatureName:any;
    FeatureNameB:any;
}
const FeatureView=(props:Props)=> {
        return (
            <View style={styles.FeatureStyle}>
                <Text style={styles.FeatureText}>{props.FeatureName}</Text>
                <Text style={styles.FeatureTextB}>{props.FeatureNameB}</Text>
            </View>
        )
    }

export default FeatureView

const styles = StyleSheet.create({
    FeatureStyle:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:wp('93%'),
        marginTop:hp('1%')
    },
    FeatureText:{
        color:'#000',
        fontWeight:'bold'
    },
    FeatureTextB:{
        color:'#0A5688',
        fontWeight:'bold'
    }
})
