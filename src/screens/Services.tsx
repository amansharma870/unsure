import React, { Component } from 'react'
import { Text, View,StyleSheet, TouchableOpacity,Image, FlatList } from 'react-native'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import Container from '../components/container'
import Icon from '../components/Icon'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Inputs_Area, Typography } from '../styles/Global'
import SliderC from '../components/ImageSliderC';
import { MainSlider } from '../styles/SliderData'
import { utils } from '../utils/utils'
import { ScreenNames } from './Dashboard'
import { useOrientation } from '../components/useOrientation'
interface Props{
  route:any
  navigation:any
}
const Services=(props:Props)=> {
  const orientation = useOrientation();

  const Haircut=()=>{
    props.navigation.navigate(ScreenNames.HAIRCUT)
}
        return (
            <Container>
                <ScrollView  showsVerticalScrollIndicator={false}>
                        <View style={styles.sectionStyle}>
                        <Icon  name={'search-outline'} size={18} style={{marginTop:hp('1.5%'),paddingLeft:7,}}
                            color={'#0A5688'}
                        />  
                        <TextInput 
                        style={styles.SearchBar}
                        placeholder="Search for salons, services.."
                        />
                        
                    </View>
                    <View>
                      <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Trending Hair Services</Text>
                    </View>
                    <SliderC 
                  onPress={Haircut}
                  images={MainSlider[3]}
                  SliderImage={{width: wp('44%'),height:hp('15%')}}
                />
                <View>
                <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                  <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Top Services</Text>
                <TouchableOpacity>
                  <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                </TouchableOpacity>
              </View>
                  { orientation === 'portrait' ? <View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.ModelA}/>  
                        <Image style={styles.Images} source={utils.Img.ModelB}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.ModelC}/>  
                        <Image style={styles.Images} source={utils.Img.ModelD}/>  
                    </View>
                    
                  </View> :  <FlatList
                                showsHorizontalScrollIndicator={false}
                                horizontal
                                data={MainSlider[5]}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity  activeOpacity={0.9} key={index}>

                                        <Image
                                        source={item.image}
                                            style={[styles.Images]}
                                        />
                                    
                                        </TouchableOpacity>
                                )}
                                keyExtractor={(item,index) => index.toString()}
                              />}
                </View>
                </ScrollView>
            </Container>
        )
    }

    const styles = StyleSheet.create({
        SearchBar: {
            ...Inputs_Area.large,
            },
            sectionStyle: {
                margin:hp('1.5%'),
                flexDirection: 'row',
                backgroundColor: '#d8d5d4ed',
                height: 40,
                borderRadius: 5,
                width:'100%',
                alignSelf:'center'
              },
              Images:{
                width: wp('44%'),
                height:hp('17%'),
                margin:wp('1.5%'),
                marginTop:hp('2%'),
                borderRadius: 5,
               },
               container: {
                // backgroundColor: '#fff',
                // borderRadius: 18,
                // width: wp('80%'),
                // height: hp('25%'),
                },
                image: {
                borderRadius: 5,
                },
            });
export default Services