import React from 'react'
import { Text, View,StyleSheet,Image, TouchableOpacity, Alert, Linking } from 'react-native'
import { Typography } from '../styles/Global'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from '../components/Icon'
import { PrimaryTheme } from '../styles/Theme'
import CustomButton from '../components/CustomButton'
import { ScrollView } from 'react-native-gesture-handler'
import { MainSlider } from '../styles/SliderData'
import SliderC from '../components/ImageSliderC';
import { useOrientation } from './useOrientation';

interface Props{
  
   
    salonName:any,
    service:any,
    star:any,
    Rating:any,
    ImageA:any,
    ImageB:any,
    ImageC:any,
    ImageD:any,
    ImageE:any,
    Discription:any,
    DaySecA:any,
    DaySecB:any,
    TimeSecA:any,
    TimeSecB:any,
    address: any,
    phone:number,
    Distance:any,
    BookAppointment:any
}
const onPress=()=>{
    return(
        Alert.alert('In Progress')
    )
}

const MySalon=(props:Props)=>{
    const orientation = useOrientation();

        return (
            <View>
                <ScrollView showsVerticalScrollIndicator={false} >
                  <Text style={[Typography.title,{color:'#000',fontSize:20,marginBottom:5,marginTop:10}]}>{props.salonName}</Text>
                  <Text style={{}}>{props.service}</Text>
                  <View style={{flexDirection:'row',marginTop:10}}>
                      <Icon size={18} color={PrimaryTheme.$MY_Theme} name={'star'}/>
                      <Icon size={18} color={PrimaryTheme.$MY_Theme} name={'star'}/>
                      <Icon size={18} color={PrimaryTheme.$MY_Theme} name={'star'}/>
                      <Icon size={18} color={PrimaryTheme.$MY_Theme} name={'star'}/>
                      <Icon size={18} color={'#D8D8D8'} name={'star'}/>
                      <Text style={{marginLeft:12}}>{props.star}</Text>
                      <Text style={{marginLeft:12}}>{props.Rating}</Text>
                  </View>
                  <View style={{flexDirection:'row',marginTop:20}}>
                      <CustomButton onPress={onPress} textStyle={{color:'#fff',fontSize:15}} buttonStyle={styles.buttonStyle} title={'About'}/>
                      <CustomButton onPress={onPress} textStyle={{color:'#0A5688',fontSize:15}} buttonStyle={styles.buttonStyleB} title={'Services'}/>
                      <CustomButton onPress={onPress} textStyle={{color:'#0A5688',fontSize:15}} buttonStyle={styles.buttonStyleB} title={'Stylists'}/>
                  </View>
                  <View style={{flexDirection:'row',marginTop:10}}>
                      <Text style={[Typography.subheading,{fontWeight:'bold'}]}>Gallary</Text>
                  </View>
                  <View style={{flexDirection:"row",justifyContent:'space-between'}}>
                    <View style={{width:wp('23%'),marginRight:10,}}>
                    <Image style={[orientation === 'portrait' ? styles.Galleryimg : Lstyles.Galleryimg]} source={props.ImageA} /> 
                    <Image style={[orientation === 'portrait' ? styles.Galleryimg : Lstyles.Galleryimg]} source={props.ImageB} />
                                            </View>
                    <View style={{width:wp('42%'),}}>
                    <Image style={[orientation === 'portrait' ? styles.centerImage : Lstyles.centerImage]} source={props.ImageC} /> 
                    
                    </View>
                    <View style={{marginLeft:12,}}>
                    <Image style={[orientation === 'portrait' ? styles.Galleryimg : Lstyles.Galleryimg]} source={props.ImageD} /> 
                    <Image style={[orientation === 'portrait' ? styles.Galleryimg : Lstyles.Galleryimg]} source={props.ImageE} />
                  </View>
                  </View>
                  <View style={{marginTop:10,borderBottomColor: '#D8D8D8',borderBottomWidth: 1,paddingBottom:28}}>
                      <Text style={[Typography.subheading,{fontWeight:'bold'}]}>Description</Text>
                      <Text>{props.Discription}</Text>
                  </View>
                 
                  <View>
                    <View style={{borderBottomColor: '#D8D8D8',borderBottomWidth: 1,paddingBottom:20}}>
                    <View style={{flexDirection:'row',marginTop:10,}}>
                        <Text style={[Typography.subheading,{fontWeight:'bold'}]}>Information</Text>
                    </View>
                         <View style={[styles.parent]}>
                            <View style={[styles.child]} >
                            <Text style={[Typography.subheading,styles.inforsec]}>{props.DaySecA}</Text>
                            <Text style={[Typography.subheading,styles.inforsec]}>{props.DaySecB}</Text>
                            </View>
                            <View style={[styles.child,]} >
                            <Text style={[Typography.subheading,styles.inforsec]}>{props.TimeSecA}</Text>
                            <Text style={[Typography.subheading,styles.inforsec]}>{props.TimeSecB}</Text>
                            </View> 
                         </View>
                         <View >
                            <View style={[styles.parent,{marginTop:12}]}>
                                <Text style={[Typography.subheading,styles.address]}>{props.address}</Text>
                                <Icon name={'navigate-circle'} color={PrimaryTheme.$MY_Theme} size={32} style={{paddingRight:8}}/>
                            </View>
                            <View>
                                <Text style={{color:PrimaryTheme.$MY_Theme}}>{props.Distance}</Text>
                                <View  style={[styles.parent,{marginTop:12}]}>
                                    <Text style={[Typography.subheading,styles.address]} onPress={()=>{Linking.openURL('tel:9115915870');} }>{props.phone}</Text>
                                    <Icon name={'call'} color={PrimaryTheme.$MY_Theme} size={32} style={{paddingRight:8}}/>
                                </View>
                            </View>
                            </View>
                    </View>
                            <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
                                <Text style={[Typography.subheading,{fontWeight:'bold'}]}>Similar Salons Nearby</Text>
                                <TouchableOpacity>
                                <Text style={{fontSize:15,color:'#0A5688',fontWeight:'bold'}}>View all</Text>
                                </TouchableOpacity>
                            </View> 
                            <View>
                            <SliderC 
                                contentContainerCustomStyle={{paddingLeft:0}}
                                images={MainSlider[2]}
                                SliderImage={{width: wp('44%'),height:hp('15%')}}
                                />
                            </View>
                            <CustomButton onPress={props.BookAppointment} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Book  Appointment'}/>
                      <View>

                      </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
    const styles = StyleSheet.create({
        buttonStyle:{
          backgroundColor: PrimaryTheme.$MY_Theme,
          borderWidth:0,
          borderColor:'#fff',width:'25%',marginRight:5,height:hp('5.5%')
        },
        buttonStyleB:{
            backgroundColor: 'transparent',
            borderWidth:0,
            borderColor:PrimaryTheme.$MY_Theme,width:'25%',marginRight:0,height:hp('5%')
       
          },
          buttonStyleC:{
            backgroundColor: PrimaryTheme.$MY_Theme,
            borderWidth:0,
            borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%'),
            marginTop:4
          },
          Galleryimg:{
            marginBottom:10,
            width: wp('21%'),
            height:hp('12.3%'),
            borderRadius:10
        },
        ButtonSingin1:{

          backgroundColor:PrimaryTheme.$MY_Theme,
          color:PrimaryTheme.$MY_Theme,
          width:wp('33%'),
          borderRadius: 10,
          height: hp('6.5%'),
          fontSize:15.3,
          padding:9,
          letterSpacing:1,
      },
      inforsec:{
          color:PrimaryTheme.$MY_Theme,
          fontSize:15,
          marginBottom:5
      },
      address:{
        color:PrimaryTheme.$MY_Theme,
        fontSize:13,
        marginBottom:5,
        width:wp('70%')
    },
      SectionStyle: {
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          borderColor: PrimaryTheme.$MY_Theme,
          height: 55,
          borderBottomWidth:0.6,
          marginBottom:10
      },
      ButtonSingin:{
  
          backgroundColor:PrimaryTheme.$MY_Theme,
          marginBottom: hp('1%'),
         color:PrimaryTheme.$MY_Theme,
         borderRadius: 10,
        },
        parent: {
            flexDirection:'row',justifyContent:'space-between'
        },child: {
            width: '48%', 
        },
        centerImage:{
            width: wp('41%'),borderRadius:10,height:hp('26%')
        }
  });
      
  const Lstyles = StyleSheet.create({
    buttonStyle:{
      backgroundColor: PrimaryTheme.$MY_Theme,
      borderWidth:0,
      borderColor:'#fff',width:'25%',marginRight:5,height:hp('5.5%')
    },
    buttonStyleB:{
        backgroundColor: 'transparent',
        borderWidth:0,
        borderColor:PrimaryTheme.$MY_Theme,width:'25%',marginRight:0,height:hp('5%')
   
      },
      buttonStyleC:{
        backgroundColor: PrimaryTheme.$MY_Theme,
        borderWidth:0,
        borderColor:'#fff',width:'100%',marginRight:5,height:hp('6%')
      },
      Galleryimg:{
        marginBottom:10,
        width: wp('50%'),
        height:hp('16%'),
        borderRadius:0
    },
    centerImage:{
         ...styles.centerImage
        ,width: wp('67.6%'),borderRadius:0,height:hp('32.2%'),margin:'1.8%'
    }
});
    export default MySalon