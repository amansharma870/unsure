import React, { useState } from "react";
import { View, StyleSheet, Image,Text, TextInput, Alert } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Container from "../components/container";
import { PrimaryTheme } from "../styles/Theme";
import { heightPercentageToDP } from "react-native-responsive-screen";
import CustomButton from "../components/CustomButton";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { utils } from "../utils/utils";
import Icon from "../components/Icon";

const ChangePWD = () => {

const Save =()=>{
  return(
    Alert.alert('Pasword Changed!')
  )
}
 
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
       <Container containerStyles={{paddingBottom:12}}>
       <View>
           <Image style={{width:wp('40%'),height:hp('30%')}} source={utils.Img.PwdCng}/>
         </View>
         <View style={styles.sectionStyleTop}>
            <Icon style={styles.searchIcon} name="lock-closed-outline" size={20} color={PrimaryTheme.$MY_Theme}/>
            <TextInput
                secureTextEntry={true}
                style={styles.input}
                placeholder="Current Password"
                underlineColorAndroid="transparent"
            />
         </View>
         <View style={styles.sectionStyle}>
            <Icon style={styles.searchIcon} name="lock-closed-outline" size={20} color={PrimaryTheme.$MY_Theme}/>
            <TextInput
                secureTextEntry={true}
                style={styles.input}
                placeholder="New Password"
                underlineColorAndroid="transparent"
            />
         </View>
         <View style={styles.sectionStyle}>
            <Icon style={styles.searchIcon} name="lock-closed-outline" size={20} color={PrimaryTheme.$MY_Theme}/>
            <TextInput
                secureTextEntry={true}
                style={styles.input}
                placeholder="Confirn Password"
                underlineColorAndroid="transparent"
            />
         </View>
            <CustomButton onPress={Save} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Change Password'}/>
 
       </Container>
    </ScrollView>
  );
};
 
const styles = StyleSheet.create({

  buttonStyleC:{
    backgroundColor: PrimaryTheme.$MY_Theme,
    borderWidth:0,
    borderColor:'#fff',width:'100%',height:hp('6%'),
    marginTop:hp('3%'),
  },
  sectionStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MY_Theme
   },
   sectionStyleTop: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MY_Theme
   },
searchIcon: {
    padding: 10,
},
input: {
    paddingRight: 10,
    paddingLeft: 0,
    color: '#424242',
},
  });


export default ChangePWD;
