
import React from 'react'
import { View,Image,StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { utils } from '../utils/utils'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context'

const Packages=()=> {
        return (
                <SafeAreaView style={{backgroundColor:'#d8d5d4ed'}}> 
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems:'center'}}>
                        <Image style={styles.Main_Images} source={utils.Img.discountD}/>  
                    </View>
                    <View style={{alignItems:'center'}}>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.PakageA}/>  
                        <Image style={styles.Images} source={utils.Img.PakageB}/>  
                        <Image style={styles.Images} source={utils.Img.PakageC}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.PakageD}/>  
                        <Image style={styles.Images} source={utils.Img.PakageA}/>  
                        <Image style={styles.Images} source={utils.Img.PakageA}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.PakageB}/>  
                        <Image style={styles.Images} source={utils.Img.PakageC}/>  
                        <Image style={styles.Images} source={utils.Img.PakageD}/>  
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.Images} source={utils.Img.PakageA}/>  
                        <Image style={styles.Images} source={utils.Img.PakageB}/> 
                        <Image style={styles.Images} source={utils.Img.PakageC}/>   
                    </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
export default Packages

const styles = StyleSheet.create({
    Main_Images:{
        width: wp('93%'),
        height:hp('20%'),
        margin:hp('2%'),
       },
   Images:{
    width: wp('28%'),
    height:hp('14%'),
    margin:hp('1%')
   },

});