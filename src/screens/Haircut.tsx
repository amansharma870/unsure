import React from 'react'
import { Text, View,StyleSheet} from 'react-native'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import Icon from '../components/Icon'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { utils } from '../utils/utils'
import SalonsData from '../components/SalonsData'
import { ScreenNames } from './Dashboard'
import ContentContainer from '../components/ContentContainer'
import { useOrientation } from '../components/useOrientation'
interface Props{
    route:any
    navigation:any
  }
const Haircut=(props:Props)=> {
    const orientation = useOrientation();

    const Orange=()=>{
        props.navigation.navigate(ScreenNames.ORANGE)
    }
    const Choco=()=>{
        props.navigation.navigate(ScreenNames.CHOCO)
    }
    const ScissorMan=()=>{
        props.navigation.navigate(ScreenNames.SCISSORMAN)
    }
        return (
            <ScrollView  showsVerticalScrollIndicator={false} >
            <ContentContainer>
                
                        <View style={[orientation === 'portrait' ? styles.sectionStyle : Lstyles.sectionStyle]}>
                        <Icon  name={'search-outline'} size={18} style={{marginTop:hp('1.5%'),paddingLeft:7,}}
                            color={'#0A5688'}
                        />  
                        <TextInput
                        style={{width:'96%'}} 
                        placeholder="Search for salons, services.."
                        />
                        
                    </View>
                    <View>
                      <Text style={{fontSize:19,color:'#000',fontWeight:'bold'}}>Salons</Text>
                    </View>
                    <SalonsData 
                       Name={'Orange The Salon'}  
                       Rating={'4.5'}
                       Venue={'Opp Courtyard Marriot, Satelite'}
                       Distance={'1.2 km'}
                       Price={'₹ 800'}
                       onPress={Orange}
                       SalonImage={utils.Img.SalonA}
                       Category={'/ Service'}/>
                    <SalonsData 
                        Name={'Choco Style You'} 
                        Rating={'4.2'}
                        Venue={'Front MDC College,California'}
                        Distance={'2 km'}
                        Price={'₹ 900'}
                        onPress={Choco}
                        SalonImage={utils.Img.SalonB}
                        Category={'/ Service'}/>
                    <SalonsData 
                        Name={'Scissor Man'} 
                        IconName={'add'} 
                        IconColor={'red'} 
                        Rating={'4.2'}
                        Venue={'Front MDC College,California'}
                        Distance={'2 km'}
                        Price={'₹ 900'}
                        onPress={ScissorMan}
                        SalonImage={utils.Img.SalonC}
                        Category={'/ Service'}/>
                <View>
                
             
                </View>
            </ContentContainer>
            </ScrollView>
        )
    }

    const styles = StyleSheet.create({

            sectionStyle: {
               marginTop:12,
               marginBottom:12,
                flexDirection: 'row',
                backgroundColor: '#d8d5d4ed',
                height: 40,
                borderRadius: 5,
                width:wp('90%'),
              },
              Images:{
                height:hp('14%'),
                margin:wp('1.5%'),
                marginTop:hp('2%')
               },
            });
            const Lstyles = StyleSheet.create({

                sectionStyle: {
                   marginTop:12,
                   marginBottom:12,
                    flexDirection: 'row',
                    backgroundColor: '#d8d5d4ed',
                    height: 40,
                    borderRadius: 5,
                  },
                  Images:{
                    height:hp('14%'),
                    margin:wp('1.5%'),
                    marginTop:hp('2%')
                   },
                });
export default Haircut