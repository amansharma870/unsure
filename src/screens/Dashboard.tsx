import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from '../components/Icon';
import { PrimaryTheme } from '../styles/Theme';
import Home from './Home';
import Cart from './Cart';10
import Booking from './Booking';
import Favourite from './Favourite';
import Profile from './Profile';
import Map from './MapView';
import TopServices from './TopServices';
import BestSalons from './BestSalons';
import Packages from './Packages';
import PopularStylists from './PopularStylist';
import Services from './Services';
import Haircut from './Haircut';
import Orange from '../salonScreens/orange';
import Choco from '../salonScreens/choco';
import ScissorMan from '../salonScreens/scissorMan';
import BookAppointMent from './BookAppointMent';
import CartB from './CartB';
import CBookings from './CBookings';
import Payment from './payment';
import ChangePWD from './ChangePWD';

interface Props{
  route:any;
  navigation:any
}


export enum ScreenNames {
  HomeSCREEN = 'HomeScreen',
  CARTSCREEN="Cart",
  BOOKINGSREEN = 'Bookings',
  FAVORITESSREEN = 'FavoritesScreen',
  PROFILESCREEN = 'ProfileScreen',

  
  CPWD='Change Password',
  CARDSCREEN_B='Payment Method',
  CBOOKINGS='Confirm Booking',
  TOPSERVICES = "Top Services",
  BESTSALONS = "Best Salons",
  STYLISTS = "PopularStylists",
  PACKAGES = "Packages",
  PAYMENT="Payment",
  MAP = "MAP",
  SERVICES="Hair",
  HAIRCUT="Haircut",
  ORANGE="Orange The Salon",
  CHOCO="Choco Style You",
  SCISSORMAN="Scissor Man",
  BOOKAPPOINTMENT="Book Appointment"
}

const HomeStack = createStackNavigator();
  function HomeStackScreen() {
    return (
        <HomeStack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}
        >
        <HomeStack.Screen  name="Home" component={Home} />
        <HomeStack.Screen name="Map" component={Map} />
        <HomeStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Top Services" component={TopServices} />
        <HomeStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }} name="Best Salons" component={BestSalons} />
        <HomeStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }} name="PopularStylists" component={PopularStylists} />
        <HomeStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'    
          }} name="Packages" component={Packages} />
          <HomeStack.Screen  options={{ 
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: '#fff'
            },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Hair" component={Services} />
          <HomeStack.Screen options={{
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: '#fff'
            },
            headerRight:()=>(
              <View style={{flexDirection:'row'}}>
               <Icon style={{margin:9,}} size={25} name={'search-outline'} color={'#0A5688'}/>
               <Icon style={{margin:9,}} size={25} name={'funnel-outline'} color={'#0A5688'}/>
            </View>
            )
         ,
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Haircut" component={Haircut}/>
          <HomeStack.Screen
            options={{ 
              headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Orange The Salon" component={Orange} />
          <HomeStack.Screen  options={{ 
            
            headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center',
          }}  name="Choco Style You" component={Choco} />
          <HomeStack.Screen  options={{ 
            
            headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center',
          }}  name="Scissor Man" component={ScissorMan} />
          <HomeStack.Screen  options={{ 
            
            headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerRight:()=>(
            <View style={{flexDirection:'row'}}>
             <Icon style={{margin:9,}} size={28} name={'cart-outline'} color={'#0A5688'}/>
          </View>
          )
       ,
          headerShown: true, headerTitleAlign: 'center',
          }}  name="Book Appointment" component={BookAppointMent} />
         
          
          
      </HomeStack.Navigator>
    );
  }
  const CartStack = createStackNavigator();
  function CardStackScreen(props:Props){
    return (
      <CartStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.HomeSCREEN)}>
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$DARK_COLOR}
            />    
            </TouchableOpacity>  
          ),    
          }}
        >
         <CartStack.Screen   options={{ 
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: '#fff'
            },
            headerRight:()=>(
              <View>
               <Icon style={{margin:9,}} size={25} name={'add'} color={'#0A5688'}/>
               
            </View>
            )
         ,
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Cart" component={Cart} />
          
          <CartStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Payment" component={CartB} />


          <CartStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Payment Method" component={Payment} />

          
          <CartStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }}  name="Confirm Booking" component={CBookings} />

      </CartStack.Navigator>
    );
}
const BookingStack = createStackNavigator();
  function BookingStackScreen(props:Props) {
    return (
      <BookingStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.CARTSCREEN)}>
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$DARK_COLOR}
            />    
            </TouchableOpacity>  
          ),       
        }}
      >
       <BookingStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }} name="Bookings" component={Booking} />
      </BookingStack.Navigator>
    );
}

const FavoritesStack = createStackNavigator();
  function FavoritesStackScreen(props:Props) {
    return (
      <FavoritesStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack('Root', { screen: 'BookingStackScreen' })
            }>
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$DARK_COLOR}
            />    
            </TouchableOpacity>  
          ),    
      }}>
        <FavoritesStack.Screen  name="Favorites" component={Favourite}  />
      </FavoritesStack.Navigator>
    );
}

const ProfileStack = createStackNavigator();
  function ProfileStackScreen(props:Props){
    return (
      <CartStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.PROFILESCREEN)}>
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$DARK_COLOR}
            />    
            </TouchableOpacity>  
          ),    
          }}
        >
        <ProfileStack.Screen  name="Profile" component={Profile}  />
        <ProfileStack.Screen  options={{ 
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: '#fff'
          },
          headerShown: true, headerTitleAlign: 'center'
          }} name="Change Password" component={ChangePWD} />
      </CartStack.Navigator>
    );
}


const Tab = createBottomTabNavigator();
  const  HairStyling =() =>{
    return (
      <>
        <Tab.Navigator  tabBarOptions={{
          activeTintColor: '#0A5688',
          inactiveTintColor: 'gray',
          style: {
            backgroundColor: '#fff'
          },
          keyboardHidesTabBar:true
        }}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ size }) => {
              if (route.name === 'Home') {
                return (
                  <Icon name={'home-outline'} size={size} color={'#0A5688'}  />
                );
              } else if (route.name === 'Cart'){
                return <Icon name={'cart-outline'} size={size} color={'#0A5688'} />;
              }else if(route.name === 'Bookings'){
                return <Icon name={'book-outline'} size={size} color={'#0A5688'} />;
              }else if(route.name === 'Favorites'){
                return <Icon name={'heart-outline'} size={size} color={'#0A5688'} />;
              }else if(route.name === 'Profile'){
                return <Icon name={'person-outline'} size={size} color={'#0A5688'} />;
              }
            },
          })}
          >
          <Tab.Screen name="Home" component={HomeStackScreen} />
          <Tab.Screen name="Cart" component={CardStackScreen}  />
          <Tab.Screen name="Bookings" component={BookingStackScreen}  />
          <Tab.Screen name="Favorites" component={FavoritesStackScreen}  />
          <Tab.Screen name="Profile" component={ProfileStackScreen}  />
        </Tab.Navigator>
      </>
    );
  }
  export default HairStyling;
