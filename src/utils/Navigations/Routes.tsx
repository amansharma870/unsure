import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Login from '../../screens/Login';
import SignIn from '../../screens/SignIn';
import {SignUp} from '../../screens/CreateAcc';
import Verify from '../../screens/VerifyAcc';
import Dashboard from '../../screens/Dashboard';


const Stack=createStackNavigator();
export enum ScreenName {
  LOGIN = 'Login',
  SIGNIN = 'SignIn',
  SIGNUP = 'SignUp',
  VERIFY = 'Verify',
  DASHBOARD = 'Dashboard',
  Map = "Map",
  TOPSERVICES = "TOPSERVICES"
}

export const authStack=()=>{
    return(
        <Stack.Navigator initialRouteName={ScreenName.LOGIN}>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.LOGIN} component={Login }/>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.SIGNIN} component={SignIn }/>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.SIGNUP} component={SignUp }/>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.VERIFY} component={Verify }/>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.DASHBOARD} component={Dashboard }/>
        </Stack.Navigator>
    )
}
