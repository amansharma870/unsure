import  { NavigationContainer } from '@react-navigation/native'
import {authStack} from './Routes'
import React from 'react'

export const Navigators =()=>{
    return(
        <NavigationContainer>
            {authStack()}
        </NavigationContainer>
    )
}
