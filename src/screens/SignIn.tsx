import React from "react";
import { Image, StyleSheet, Text, View ,Linking, StatusBar} from "react-native";
import CustomButton from "../components/CustomButton";
import PhoneInput from "react-native-phone-number-input";
import { ScreenName } from "../utils/Navigations/Routes";
import { Div_Area, Spacing, Typography } from "../styles/Global";
import Container from "../components/container";
import { utils } from "../utils/utils";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from "react-native-gesture-handler";
import { useOrientation } from "../components/useOrientation";

interface Props{
    route:any
    navigation:any
  }
const SignIn=(props:Props)=>{
    const orientation = useOrientation();

    const Verify = () =>{
        props.navigation.navigate(ScreenName.VERIFY)
        }
    return(
        <ScrollView showsVerticalScrollIndicator={false}>
            <Container  containerStyles={[orientation === 'portrait' ? styles.mystyle : Lstyles.mystyle]}>
            <StatusBar
                animated={true}
                backgroundColor="#0A5688"
            />
            
                <View style={[orientation === 'portrait' ? styles.atall : Lstyles.atall]}>
                <View style={{alignItems:'center'}}>
                    <Text style={[Typography.title]}>Sign In</Text>
                </View>
                <View style={{marginBottom:Spacing.regular.marginBottom}}>
                <Image style={[Div_Area.title_Image]} source={utils.Img.SignIn}/>  
                </View>
                </View>
                <View style={Div_Area.regular}>
                    <Text style={Typography.heading}>Welcome to Beauty Salon</Text>
                    <Text style={Typography.subheading}>Enter your mobile number to get One Time Password.</Text>
                    <View style={Div_Area.regular}>
                        <PhoneInput
                            containerStyle={{marginBottom:10,width:wp('90%')}}
                            textInputStyle={Div_Area.Phone_Text}
                            placeholder="Enter phone number" 
                        />
                    </View>
                    <CustomButton onPress={Verify} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleA} title={'Get OTP'}/>
                    <CustomButton onPress={() => Linking.openURL('http://google.com')} textStyle={{color:'#909090'}} buttonStyle={styles.buttonStyleB} title={'Connect with Google'}/>
                </View>   
            
            </Container>
        </ScrollView>
    )
}

const styles = StyleSheet.create({

    buttonStyleB:{
        backgroundColor: 'transparent',
        borderWidth:1,
        borderColor:'#909090'
      },
      buttonStyleA:{
        backgroundColor: '#0A5688',
        borderWidth:1,
        borderColor:'#909090'
      },
      mystyle:{
        marginTop:30
      },
      atall:{
        padding:0
    }
});
const Lstyles = StyleSheet.create({

    buttonStyleB:{
        backgroundColor: 'transparent',
        borderWidth:1,
        borderColor:'#909090'
      },
      buttonStyleA:{
        backgroundColor: '#0A5688',
        borderWidth:1,
        borderColor:'#909090'
      },
      mystyle:{
        flexDirection:'row',
        height:hp('53%')
    },
    atall:{
        padding:20
    }
});

export default SignIn;